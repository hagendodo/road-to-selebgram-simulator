import java.util.Scanner;
import java.util.Random;

class SocialMedia{
    boolean checkReport(Account pengguna){
        if(pengguna.report > 50){
            pengguna.status = false;
            this.banned(pengguna);
            return false;
        }else if(pengguna.follower > 100){
            pengguna.verified = true;
            this.checkVerified(pengguna);
            return false;
        }
        return true;
    }
    
    void checkVerified(Account pengguna){
        if(pengguna.isVerify()){
           System.out.println("Selamat, kamu akun mu telah verified [TAMAT].");
        }
    }
    
    void banned(Account pengguna){
        if(pengguna.isBanned()){
            System.out.println("Akun kamu telah di ban! kamu tidak dapat melakukan aktivitas apapun [GAME OVER].");
        }
    }
}

class Account extends SocialMedia{
    String username;
    int follower;
    int report;
    int konten;
    boolean klarifikasi;
    boolean status;
    boolean verified;
    
    Account(String username){
        this.username = username;
        this.follower = 0;
        this.report = 0;
        this.konten = 0;
        this.verified = false;
        this.status = true;
    }
    
    void postKonten(){
        if(isBanned()){
            return;
        }
        int tempFollower = this.follower, tempReport = this.report;
        this.konten++;
        this.follower += new Random().nextInt(10 + 1);
        this.report += new Random().nextInt(5 + 1);
        this.klarifikasi = false;
           
        System.out.println("Selamat! kamu berhasil post konten ke-"+this.konten);
        System.out.println("Followers bertambah : "+(this.follower-tempFollower));
        System.out.println("Report bertambah : "+(this.report-tempReport));
    }
    
    void klarifikasi(){
        if(isBanned()){
            return;
        }
        if(!isKlarifikasi()){
            int randomFollower = new Random().nextInt(3 + 1);
            int randomReport = new Random().nextInt(3 + 1);
            int tempFollower = this.follower, tempReport = this.report;
            
            this.follower -= randomFollower;
            this.report -= randomReport;
            this.klarifikasi = true;
            
            if(randomFollower > randomReport){
                System.out.println("Klrafikasi berhasil!");
                System.out.println("Followers berukurang : "+(this.follower-tempFollower));
        System.out.println("Report berkurang : "+(this.report-tempReport));
            }else{
                System.out.println("Sepertinya klarifikasi kali ini gagal!");
                System.out.println("Followers berkurang : "+(this.follower-tempFollower));
        System.out.println("Report berkurang : "+(this.report-tempReport));
            }
        }else{
            System.out.println("Kamu sudah menggunakan jatah klarifikasi!");
        }
    }
    
    private boolean isKlarifikasi(){
        return this.klarifikasi;
    }
    
    boolean isBanned(){
        return !this.status;
    }
    
    boolean isVerify(){
        return this.verified;
    }
    
    void cekStatistik(){
        System.out.println("Username : "+this.username);
        System.out.println("Followers : "+this.follower);
        System.out.println("Report : "+this.report);
        if(!this.isVerify()){
            System.out.println("Status : Belum verified");
        }
    }
    
}

public class RoadToSelebgramSimulator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        SocialMedia instagram = new SocialMedia();
        Account ibnu = new Account("ibnu");
        String pilihan;
        boolean game_status;
        do{
            System.out.println("----Road To Selebgram Simulator----");
            System.out.print("Pilihan : ");
            pilihan = input.nextLine();
            
            switch(pilihan.charAt(0)){
                case 'k':
                    ibnu.klarifikasi();
                    break;
                case 'p':
                    ibnu.postKonten();
                    break;
                case 's':
                    ibnu.cekStatistik();
                    break;
                default:
                    System.out.println("Pilihan tidak valid!");
            }
            System.out.println("-------------------------------");
            game_status = instagram.checkReport(ibnu);
        }while(game_status);
        
        
    }
}