# Road To Selebgram Simulator

## Version
Versi 1.0

## Description

Mini game simulasi sederhana menjadi seorang selebgram.

## Prerequisite
- Java Runtime Environment

## How to Install
Open terminal and do:
```
cd YOUR_FILE_DOWNLOAD_PATH/road-to-selebgram-simulator/
javac RoadToSelebgramSimulator.java
java RoadToSelebgramSimulator
```

## How to Play
Tujuan dari game ini adalah untuk mendapatkan verified dan jangan sampai di banned.

Setiap post konten, follower dan report akan bertambah secara acak. Apabila jumlah follower > 100 maka kamu mendapatkan verified, tetapi jika jumlah report > 50 maka kamu akan di banned.

Jika ingin mengurangi jumlah report bisa melakukan klarifikasi, dengan resiko kemungkinan follower berkurang. Batas untuk klarifikasi adalah 1x untuk setiap konten yang di post dan direset jika post konten baru.

list instruksi:
```
p : post konten
k : klarifikasi
s : cek statistik
```
## Authors
Raden Ibnu Huygenz Widodo (1217050116).

## License
Open Source.
